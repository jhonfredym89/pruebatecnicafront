angular.module("appPruebaFront")
	.factory("pacientesFactory", ["localStorageService", function(lss){
		var pacienteFactory = {};
		pacienteFactory.key = "keyPacientes";
		if (lss.get(pacienteFactory.key)) {
			pacienteFactory.pacientes = lss.get(pacienteFactory.key);
		} else {
			pacienteFactory.pacientes = [{
				nhc : 1,
				nombre : "Jhon Fredy",
				primerApellido : "Martinez",
				segundoApellido : "Realpe",
				genero : "M",
				fechaNacimiento : new Date(1989,04,15,00,00,00),
				nif : "1061718913",
				direccion : {
					calle : "Calle 18",
					numero : "4-03",
					puerta : "1",
					codigoPostal : "0001",
					ciudad : "Popayán"
				},
				aseguradoras : [{
					nombre : "Surameris",
					tipo : "Familiar",
					tarjeta : 123456
				}, {
					nombre : "Aseguradora del estado",
					tipo : "Salud",
					tarjeta : 654321
				}]
			}, {
				nhc : 2,
				nombre : "Maria",
				primerApellido : "Dolores",
				genero : "F",
				fechaNacimiento : new Date(1967,01,10,00,00,00),
				nif : "AC123456",
				direccion : {
					calle : "Carrera 11A",
					numero : "13A-18",
					puerta : "1",
					codigoPostal : "0001",
					ciudad : "Popayán"
				},
				aseguradoras : [{
					nombre : "Surameris",
					tipo : "Dental",
					tarjeta : 456789
				}]
			}, {
				nhc : 3,
				nombre : "Antonieta",
				primerApellido : "Valderrama",
				segundoApellido : "Rincon",
				genero : "F",
				nif : "AC987654",
				direccion : {
					calle : "Carrera 53",
					numero : "13A-60",
					puerta : "203C",
					codigoPostal : "85321",
					ciudad : "Cali"
				}
			}, {
				nhc : 4,
				nombre : "Fredy",
				primerApellido : "Realpe",
				aseguradoras : [{
					nombre : "Surameris",
					tipo : "Salud",
					tarjeta : 987654
				}, {
					nombre : "Aseguradora del estado",
					tipo : "Familiar",
					tarjeta : 654321
				}]
			}];
		}
		
		pacienteFactory.addPaciente = function(paciente) {
			pacienteFactory.pacientes.push(paciente);
			pacienteFactory.updateLocalStorage();
		}
		
		pacienteFactory.removePaciente = function(paciente) {
			pacienteFactory.pacientes = pacienteFactory.pacientes.filter(function(item) {
				return paciente.nhc !== item.nhc;
			});
			pacienteFactory.updateLocalStorage();
			return pacienteFactory.getAll();
		}
		
		pacienteFactory.updateLocalStorage = function() {
			lss.set(pacienteFactory.key, pacienteFactory.pacientes);
		}
		
		pacienteFactory.getAll = function() {
			return pacienteFactory.pacientes;
		}
		
		pacienteFactory.updatePaciente = function() {
			pacienteFactory.updateLocalStorage();
		}
		
		return pacienteFactory;
	}])
	.factory("profesionalesFactory", ["localStorageService", function(lss){
		var profesionalFactory = {};
		profesionalFactory.key = "keyProfesionales";
		if (lss.get(profesionalFactory.key)) {
			profesionalFactory.profesionales = lss.get(profesionalFactory.key);
		} else {
			profesionalFactory.profesionales = [{
				numeroColegiado : 123,
				nombre : "Tatiana",
				primerApellido : "Aristizabal",
				genero : "F",
				nif : "PAS741",
				tipo : "Médico",
				direccion : {
					calle : "Calle 53",
					numero : "1-60",
					puerta : "2",
					codigoPostal : "85321",
					ciudad : "Cali"
				}
			}, {
				numeroColegiado : 456,
				nombre : "Bairon Santiago",
				primerApellido : "Freire",
				segundoApellido : "Catamuscay",
				genero : "M",
				fechaNacimiento : new Date(1970,02,01,00,00,00),
				nif : "AC741",
				tipo : "Enfermero",
				direccion : {
					calle : "Carrera 3",
					numero : "10-89",
					puerta : "21",
					codigoPostal : "741",
					ciudad : "Bogotá"
				}
			}, {
				numeroColegiado : 789,
				nombre : "Manuel Elkin",
				primerApellido : "Patarroyo",
				segundoApellido : "Murillo",
				genero : "M",
				fechaNacimiento : new Date(1946,03,15,00,00,00),
				nif : "49613572",
				tipo : "Médico"
			}, {
				numeroColegiado : 741,
				nombre : "Adriana",
				primerApellido : "Arboleda",
				genero : "F",
				tipo : "Administrativo"
			}, {
				numeroColegiado : 258,
				nombre : "Rocky",
				primerApellido : "Balvoa",
				genero : "M",
				nif : "PAS720912",
				tipo : "Enfermero",
				direccion : {
					calle : "Calle 83",
					numero : "6-810",
					puerta : "1",
					ciudad : "Medellin"
				}
			}];
		}
		
		profesionalFactory.addProfesional = function(profesional) {
			profesionalFactory.profesionales.push(profesional);
			profesionalFactory.updateLocalStorage();
		}
		
		profesionalFactory.removePaciente = function(profesional) {
			profesionalFactory.profesionales = profesionalFactory.profesionales.filter(function(item) {
				return profesional.numeroColegiado !== item.numeroColegiado;
			});
			profesionalFactory.updateLocalStorage();
			return profesionalFactory.getAll();
		}
		
		profesionalFactory.updateLocalStorage = function() {
			lss.set(profesionalFactory.key, profesionalFactory.profesionales);
		}
		
		profesionalFactory.getAll = function() {
			return profesionalFactory.profesionales;
		}
		
		profesionalFactory.deleteMedicos = function() {
			profesionalFactory.profesionales = profesionalFactory.profesionales.filter(function(item) {
				return item.tipo !== "Médico";
			});
			profesionalFactory.updateLocalStorage();
			return profesionalFactory.getAll();
		}
		
		return profesionalFactory;
	}]);