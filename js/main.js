function openTab(evt, idTab, tablinks, tabcontent) {
    var i, tabcont, tablink;
    tabcont = document.getElementsByClassName(tabcontent);
    for (i = 0; i < tabcont.length; i++) {
        tabcont[i].style.display = "none";
    }
    tablink = document.getElementsByClassName(tablinks);
    for (i = 0; i < tablink.length; i++) {
        tablink[i].className = tablink[i].className.replace(" active", "");
    }
    document.getElementById(idTab).style.display = "block";
    evt.currentTarget.className += " active";
}