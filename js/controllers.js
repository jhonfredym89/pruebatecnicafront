angular.module("appPruebaFront")
	.controller("usuariosController", ["$scope", "pacientesFactory", "profesionalesFactory", "$modal", function(sc, pacF, proF, m) {
		sc.tituloPacientes = "Pacientes";
		sc.tituloProfesionales = "Profesional";
		sc.pacientes = pacF.getAll();
		sc.profesionales = proF.getAll();
		sc.tipoUsuario = "";
		sc.paciente = {};
		sc.profesional = {};
		sc.aseguradoras = [];
		sc.aseguradora = {};
		
		sc.adicionarAseguradora = function() {
			sc.aseguradoras.push(sc.aseguradora);
			sc.aseguradora = {};
		}
		
		sc.eliminarAseguradora = function(ase) {
			sc.aseguradoras = sc.aseguradoras.filter(function(item) {
				return ase !== item;
			});
		}
		
		sc.adicionarUsuario = function () {
			if (sc.tipoUsuario == "paciente") {
				sc.paciente.aseguradoras = sc.aseguradoras;
				pacF.addPaciente(sc.paciente);
				sc.paciente = {};
				sc.aseguradora = {};
				sc.aseguradoras = [];
			} else {
				proF.addProfesional(sc.profesional);
				sc.profesional = {};
			}
		}
		
		sc.eliminarPaciente = function(paciente) {
			sc.pacientes = pacF.removePaciente(paciente);
		}
		
		sc.eliminarProfesional = function(profesional) {
			sc.profesionales = proF.removePaciente(profesional);
		}
		
		sc.eliminarMedicos = function() {
			var opcion = confirm("¿Está seguro que desea borrar los médicos?")
			if (opcion == true) {
				sc.profesionales = proF.deleteMedicos();
			}
		}
	}])
	.controller("usuarioController", ["$scope", "$location", "pacientesFactory", "profesionalesFactory", "$routeParams", function(sc, loc, pacF, proF, rp) {
		sc.tipoUsuario = rp.tipo;
		sc.aseguradora = {};
		
		if (rp.soloLectura == "true") {
			sc.soloLectura = true;
		}else {
			sc.soloLectura = false;
		}
		
		if (rp.tipo == "paciente") {
			var pacientes = pacF.getAll();
			pacientes.forEach(function(item) {
				if(item.nhc == rp.id) {
					sc.paciente = item;
					if (item.aseguradoras === null || item.aseguradoras === undefined) {
						sc.aseguradoras = [];
					} else {
						sc.aseguradoras = item.aseguradoras;
					}
					return false;
				}
			});
		} else {
			var profesionales = proF.getAll();
			profesionales.forEach(function(item) {
				if(item.numeroColegiado == rp.id) {
					sc.profesional = item;
					return false;
				}
			});
		}
		
		sc.adicionarAseguradora = function() {
			sc.aseguradoras.push(sc.aseguradora);
			sc.aseguradora = {};
		}
		
		sc.eliminarAseguradora = function(ase) {
			sc.aseguradoras = sc.aseguradoras.filter(function(item) {
				return ase !== item;
			});
		}
		
		sc.editarUsuario = function () {
			if (sc.tipoUsuario == "paciente") {
				sc.paciente.aseguradoras = sc.aseguradoras;
				pacF.updatePaciente();
				sc.aseguradora = {};
				loc.url("/");
			} else {
				proF.updateProfesional(sc.profesional);
			}
		}
	}]);