angular.module("appPruebaFront", ["LocalStorageModule", "ngRoute"])
	.config(["$routeProvider", function(rp) {
		rp.when("/", {
			controller : "usuariosController",
			templateUrl : "views/home.html"
		})
		.when("/usuario", {
			controller : "usuariosController",
			templateUrl : "views/usuario.html"
		})
		.when("/editar/:tipo/:id/:soloLectura", {
			controller : "usuarioController",
			templateUrl : "views/editar.html"
		});
	}]);